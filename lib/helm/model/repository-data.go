package model

type RepositoryData struct {
	ApiVersion string                 `yaml:"apiVersion"`
	Entries    map[string][]ChartItem `yaml:"entries"`
}
