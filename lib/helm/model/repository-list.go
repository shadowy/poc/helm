package model

import (
	"gopkg.in/yaml.v3"
	"io/ioutil"
)

type RepositoryList struct {
	Repositories []RepositoryInfo `yaml:"repositories"`
}

func (l RepositoryList) FromFile(cfg string) ([]string, error) {
	data, err := ioutil.ReadFile(cfg)
	if err != nil {
		return nil, err
	}
	t := RepositoryList{}
	err = yaml.Unmarshal(data, &t)
	if err != nil {
		return nil, err
	}
	var list []string
	for i := range t.Repositories {
		list = append(list, t.Repositories[i].Name)
	}
	return list, nil
}
