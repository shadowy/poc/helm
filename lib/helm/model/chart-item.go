package model

type ChartItem struct {
	Name    string `yaml:"name"`
	Version string `yaml:"version"`
}
