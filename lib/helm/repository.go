package helm

import (
	"gitlab.com/shadowy/go/poc/helm/lib/helm/model"
	"gopkg.in/yaml.v3"
	"io/ioutil"
)

type Repository struct {
	Name  string
	Chart map[string]string
}

func (r Repository) FromFile(folder, name string) (*Repository, error) {
	res := Repository{
		Name:  name,
		Chart: map[string]string{},
	}

	data, err := ioutil.ReadFile(folder + "/" + name + "-index.yaml")
	if err != nil {
		return nil, err
	}
	t := model.RepositoryData{}
	err = yaml.Unmarshal(data, &t)
	if err != nil {
		return nil, err
	}
	for key := range t.Entries {
		item := t.Entries[key]
		res.Chart[key] = item[0].Version
	}
	return &res, nil
}

func (r Repository) ListFromFile(cfg, folder string) (map[string]*Repository, error) {
	list, err := model.RepositoryList{}.FromFile(cfg)
	if err != nil {
		return nil, err
	}
	res := map[string]*Repository{}
	for i := range list {
		k := list[i]
		res[k], err = Repository{}.FromFile(folder, k)
		if err != nil {
			return nil, err
		}
	}
	return res, nil
}
