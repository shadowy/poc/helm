package config

import (
	"github.com/mittwald/go-helm-client"
	"gopkg.in/yaml.v3"
	"helm.sh/helm/v3/pkg/repo"
	"io/ioutil"
)

type Settings struct {
	Repositories []Repository `yaml:"repositories"`
	Releases     []Release    `yaml:"releases"`
}

func (s Settings) FromFile(file string) (*Settings, error) {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	var res Settings
	err = yaml.Unmarshal(data, &res)
	if err != nil {
		return nil, err
	}
	return &res, nil
}

func (s *Settings) AddRepositories(client helmclient.Client) error {
	for i := range s.Repositories {
		item := s.Repositories[i]
		chartRepo := repo.Entry{
			Name:               item.Name,
			URL:                item.URL,
			Username:           item.Username,
			Password:           item.Password,
			PassCredentialsAll: true,
		}
		if err := client.AddOrUpdateChartRepo(chartRepo); err != nil {
			return err
		}
	}
	return nil
}

func (s *Settings) GetRelease(name string) *Release {
	for i := range s.Releases {
		item := s.Releases[i]
		if item.Name == name {
			return &item
		}
	}
	return nil
}
