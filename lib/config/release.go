package config

type Release struct {
	Name       string `yaml:"name"`
	Repository string `yaml:"repository"`
	Chart      string `yaml:"chart"`
}
