package main

import (
	"bytes"
	"context"
	"flag"
	"github.com/mittwald/go-helm-client"
	"github.com/rs/zerolog/log"
	"gitlab.com/shadowy/go/poc/helm/lib/config"
	"gitlab.com/shadowy/go/poc/helm/lib/helm"
	_ "gitlab.com/shadowy/go/zerolog-settings"
)

var settingsFile = flag.String("settings", "./helm-update.yaml", " file with settings")
var repositoryCache = flag.String("repository-cache", "/tmp/.helmcache", "helm cache")
var repositoryConfig = flag.String("repository-config", "/tmp/.helmrepo", "helm config")

func main() {
	flag.Parse()
	l := log.Logger.With().
		Str("settings-file", *settingsFile).
		Str("repository-cache", *repositoryCache).
		Str("repository-config", *repositoryConfig).
		Logger()
	l.Info().Msg("helm-update")
	settings, err := config.Settings{}.FromFile(*settingsFile)
	if err != nil {
		l.Error().Err(err).Msg("load settings")
		panic(err)
	}
	var outputBuffer bytes.Buffer

	opt := &helmclient.Options{
		Namespace:        "default",
		RepositoryCache:  *repositoryCache,
		RepositoryConfig: *repositoryConfig,
		Debug:            true,
		Linting:          true,
		DebugLog: func(format string, v ...interface{}) {
			l.Debug().Str("format", format).Msg("Helm")
		},
		Output: &outputBuffer,
	}

	helmClient, err := helmclient.New(opt)
	if err != nil {
		l.Error().Err(err).Msg("helm client: New")
		panic(err)
	}

	if err = settings.AddRepositories(helmClient); err != nil {
		l.Error().Err(err).Msg("add repositories")
		panic(err)
	}

	repositories, err := helm.Repository{}.ListFromFile(opt.RepositoryConfig, opt.RepositoryCache)
	if err != nil {
		l.Error().Err(err).Msg("load data from repositories")
		panic(err)
	}

	l.Info().Msg("get the list of installed charts")
	list, err := helmClient.ListDeployedReleases()
	if err != nil {
		l.Error().Err(err).Msg("get the list of installed charts")
		panic(err)
	}
	for i := range list {
		item := list[i]
		li := l.With().
			Str("name", item.Name).
			Str("ver", item.Chart.Metadata.Version).
			Str("status", item.Info.Status.String()).
			Logger()
		if release := settings.GetRelease(item.Name); release != nil {
			newVersion := repositories[release.Repository].Chart[release.Chart]
			if newVersion != item.Chart.Metadata.Version {
				li.Debug().Str("new-ver", newVersion).Msg("chart")
				chartSpec := helmclient.ChartSpec{
					ReleaseName: release.Name,
					ChartName:   release.Chart,
					Namespace:   item.Namespace,
					UpgradeCRDs: true,
					Wait:        true,
				}
				release, err := helmClient.UpgradeChart(context.Background(), &chartSpec, nil)
				if err != nil {
					li.Error().Err(err).Msg("chart upgrade")
				} else {
					li.Debug().Str("new-version", release.Chart.Metadata.Version).Msg("chart upgraded")
				}
			} else {
				li.Debug().Msg("char up to date")
			}
		}
	}
}
